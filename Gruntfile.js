module.exports = function (grunt) {

    // Load all tasks
    require('load-grunt-tasks')(grunt);

    // Show elapsed time
    require('time-grunt')(grunt);

    var vendorDir = 'bower_components',

        sharedJsList = [
            /* FRAMEWORKS */
        ],
        jsFileList = [
            sharedJsList
        ],
        lessFileList = [
            '_less/main.less'
        ];

    // Project configuration.
    grunt.initConfig(
        {
            shell: {
                jekyllBuild: {
                    command: 'jekyll build'
                },
                jekyllServe: {
                    command: 'jekyll serve'
                }
            },
            concat: {
                options: {
                    separator: '\n;'
                },
                main: {
                    src: [jsFileList],
                    dest: 'build/main.dev.js'
                }
            },
            uglify: {
                main: {
                    files: {
                        'build/main.min.js': [jsFileList]
                    }
                }
            },
            less: {
                main_dev: {
                    files: {
                        'build/main.dev.css': lessFileList
                    },
                    options: {
                        compress: false
                    }
                },
                main_prod: {
                    files: [
                        {dest: 'build/main.min.css', src: lessFileList}
                    ],
                    options: {
                        compress: true
                    }
                }
            },
            autoprefixer: {
                options: {
                    browsers: ['last 2 versions', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
                },
                main_dev: {
                    src: 'build/main.dev.css'
                },
                main_prod: {
                    src: ['build/main.min.css']
                }
            },
            htmlbuild: {
                main: {
                    src: '_site/**/index.html',
                    dest: '_site/build/',
                    options: {
                        replace: true,
                        prefix: '/',
                        scripts: {
                            app: [
                                '_site/build/main.min.js'
                            ]
                        },
                        styles: {
                            app: [
                                '_site/build/app.min.css'
                            ]
                        }
                    }
                }
            },
            replace: {
                icomoon: {
                    options: {
                        patterns: [
                            {
                                match: 'fonts/icomoon',
                                replacement: '/assets/icomoon/fonts/icomoon'
                            },
                            {
                                match: '[class^="icon-"], [class*=" icon-"]',
                                replacement: '.icon:before, [class^="icon-"]:before, [class*=" icon-"]:before'
                            }
                        ],
                        usePrefix: false
                    },
                    files: [
                        {
                            expand: false,
                            flatten: true,
                            src: ['assets/icomoon/style.css'],
                            dest: 'build/icomoon.css.tmp'
                        }
                    ]
                }
            },
            cacheBust: {
                options: {
                    baseDir: '_site',
                    rename: false
                },
                main: {
                    files: [
                        {
                            src: ['_site/**/index.html']
                        }
                    ]
                }
            },
            watch: {
                main_less: {
                    files: 'assets/**/*.less',
                    tasks: ['less:main_dev', 'autoprefixer:main_dev']
                },
                main_js: {
                    files: jsFileList,
                    tasks: ['concat:main']
                }
            }
        }
    );

    // Register tasks
    grunt.registerTask('default', ['development']);

    grunt.registerTask('development', [
        //'jshint',
        //'copy:iconFonts',
        //'copy:modernizr',
        //'less:dev_main',
        //'less:dev_admin',
        //'replace:icomoon',
        'less:main_dev',
        'autoprefixer:main_dev',
        'concat'
    ]);

    grunt.registerTask('production', [
        'html2js',
        'uglify',
        'replace:icomoon',
        'less:build',
        'htmlbuild',
        'cacheBust'
    ]);

};