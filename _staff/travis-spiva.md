---
layout: staff-profile
first_name: Travis
last_name: Spiva
display_name: 'Travis Spiva'
email: TravisS@faithpromise.org
phone: null
title: 'Campbell Worship Pastor'
teams: campbell
sort: '2'
test: [{ name: foo }, { name: bar }, { name: test }]
---

