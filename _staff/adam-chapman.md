---
layout: staff-profile
first_name: Adam
last_name: Chapman
display_name: 'Adam Chapman'
email: AdamC@faithpromise.org
phone: null
title: 'Video Project Manager'
teams: worship
sort: '11'
test: [{ name: foo }, { name: bar }, { name: test }]
---

