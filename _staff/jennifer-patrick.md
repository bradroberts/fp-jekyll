---
layout: staff-profile
first_name: Jennifer
last_name: Patrick
display_name: 'Jennifer Patrick'
email: JenniferP@faithpromise.org
phone: '1403'
title: 'Group Assimilation Coordinator'
teams: groups
sort: '35'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Jennifer has been attending Faith Promise Church since January 2002. Soon afterwards, she and her family attended a small group, and that began her love for small group ministry. She started out as a volunteer in small group ministry, and she's been on staff since 2005.

In her free time, she and her husband, Rob love to spend time at the lake with friends and family and make frequent visits to the Smoky mountains.