---
layout: staff-profile
first_name: Macy
last_name: Deel
display_name: 'Macy Deel'
email: MacyD@faithpromise.org
phone: '1711'
title: 'Next Steps Assistant'
teams: administration
sort: '35'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Hi! My name is Macy Deel, and I've been attending Faith Promise for the past two and a half years with my family. I grew up in church, and have had a relationship with the Lord since an early age. I was even blessed enough to have Miles Creasman, our Next Steps Pastor/Blount Campus Groups Pastor, as my youth pastor while growing up!

Since first attending Faith Promise I have felt so connected and tied to this church, and it has helped me to continually work on strengthening my relationship with Christ. I also volunteer in fpKids, our children's ministry, and have loved giving children the opportunity to learn about the Lord. I am so excited about the plans that God has for Faith Promise Church and spreading His Word, and I am thankful to be a part of it!

I've lived in Knoxville since I was four years old, graduated from Bearden High School, and am currently a senior at UT. I'm studying Psychology and Child and Family Studies. Although I'm not yet sure what I'd like to do with my major, I trust that God will place me right where He wants me!

Outside of school, I enjoy spending time with my friends and family and going to the pool and lake. I also love seeing movies, drinking Coca Cola, playing the Wii, going to Disneyworld, eating Italian food, and traveling.