---
layout: staff-profile
first_name: Deneen
last_name: Lambert
display_name: 'Deneen Lambert'
email: DeneenL@faithpromise.org
phone: '1600'
title: 'Assistant to the Chief Financial Officer'
teams: administration
sort: '11'
test: [{ name: foo }, { name: bar }, { name: test }]
---

