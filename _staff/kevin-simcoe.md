---
layout: staff-profile
first_name: Kevin
last_name: Simcoe
display_name: 'Kevin Simcoe'
email: KevinS@faithpromise.org
phone: '1411'
title: 'North Knox Groups Pastor'
teams: north
sort: '100'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Kevin moved to Knoxville with his family in 1996. After attending and serving in a number of roles at a local church for 11 years, he and his family felt a call to serve God in new and deeper ways and were led to Faith Promise in 2007.

From 2008-2012 he served in leadership with men's groups and events. In 2011 he joined the leadership team of fpCelebrate where he continues to serve today. In December of 2012, Kevin joined the staff in a volunteer role as the Groups Pastor at the North Knox Campus.

Kevin lives in West Knoxville with his wife of 26 years (Isabell) and his three sons; Ryan, Travis, and Graham.