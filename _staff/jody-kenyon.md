---
layout: staff-profile
first_name: Jody
last_name: Kenyon
display_name: 'Jody Kenyon'
email: JodyK@faithpromise.org
phone: '1400'
title: 'Group Ministries'
teams: groups
sort: '34'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Jody and her family have been at Faith Promise since the beginning of the church, and she came on staff in February 2001.

Her favorite food is seafood, least favorite food is peas, and greatest fear is snakes.

She has two children, Brandon and Brianna and two beautiful grandchildren, Greyson and Saylor.

Favorite hobbies:
Spending time with her family and traveling anywhere warm and coastal!