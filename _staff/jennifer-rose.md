---
layout: staff-profile
first_name: Jennifer
last_name: Rose
display_name: 'Jennifer Rose'
email: JenniferR@faithpromise.org
phone: '1100'
title: 'Assistant to the Pastor of Leadership Development & Stewardship'
teams: leadership
sort: '6'
test: [{ name: foo }, { name: bar }, { name: test }]
---

