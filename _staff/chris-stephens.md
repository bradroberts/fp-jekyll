---
layout: staff-profile
first_name: Chris
last_name: Stephens
display_name: 'Dr. Chris Stephens'
email: Pastor@faithpromise.org
phone: '2000'
title: 'Senior Pastor'
teams: executive
sort: '1'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Pastor Chris was born in Chattanooga, Tennessee to teen parents who split up when he was only three.

By the age of 22 he was a heavy drug user and dealer. Later that year, Chris woke up in a hospital room after a drug overdose and made a decision to connect with God. This decision was especially amazing since neither Chris nor his family had ever attended church.

Over the next 26 years Chris has been serving God as a speaker and a pastor. He has had the opportunity to speak all over the world and has impacted tens of thousands of lives.

Pastor Chris has written a biographical look at his life and the transformation that took place. The book is called The Climb of Your Life. Chris said if his life could transition from the project to the pulpit; from a life of devastation to an amazing destiny, then so can any life.

As the Senior Pastor of Faith Promise Church, he has seen thousands find hope and fulfillment in their destiny. Chris says his purpose is to help people recognize and achieve their full potential. He came so close to death and missing all God had for him he now wants to help others find the same purpose in their lives that he has found.

Pastor's blog and information about his book can be found at <a href="http://drchrisstephens.com">DrChrisStephens.com</a>.

You can also connect with Pastor Chris via <a href="http://facebook.com/pastorchris">Facebook</a> and <a href="http://twitter.com/drchrisstephens">Twitter</a>.