---
layout: staff-profile
first_name: Chris
last_name: Looper
display_name: 'Chris Looper'
email: ChrisL@faithpromise.org
phone: null
title: 'fpStudents Venue & Creative Director'
teams: family
sort: '62'
test: [{ name: foo }, { name: bar }, { name: test }]
---

