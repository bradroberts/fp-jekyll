---
layout: staff-profile
first_name: Tanner
last_name: Dalton
display_name: 'Tanner Dalton'
email: TannerD@faithpromise.org
phone: null
title: 'Central Worship Associate'
teams: worship
sort: '31'
test: [{ name: foo }, { name: bar }, { name: test }]
---

