---
layout: staff-profile
first_name: Jennifer
last_name: Spencer
display_name: 'Jennifer Spencer'
email: JenniferS@faithpromise.org
phone: null
title: 'Volunteer Coordinator of Support Teams'
teams: family
sort: '12'
test: [{ name: foo }, { name: bar }, { name: test }]
---

