---
layout: staff-profile
first_name: Joe
last_name: Filipowicz
display_name: 'Joe Filipowicz'
email: JoeF@faithpromise.org
phone: '1605'
title: 'IT Director'
teams: administration
sort: '47'
test: [{ name: foo }, { name: bar }, { name: test }]
---

