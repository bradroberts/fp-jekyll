---
layout: staff-profile
first_name: Jordan
last_name: Peltz
display_name: 'Jordan Peltz'
email: JordanP@faithpromise.org
phone: null
title: 'Video Project Support'
teams: worship
sort: '12'
test: [{ name: foo }, { name: bar }, { name: test }]
---

