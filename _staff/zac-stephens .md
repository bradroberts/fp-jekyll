---
layout: staff-profile
first_name: Zac
last_name: Stephens
display_name: 'Zac Stephens'
email: ZacS@faithpromise.org
phone: '1304'
title: 'Pastor of Student Ministries/Pellissippi Campus Student Pastor'
teams: family
sort: '60'
test: [{ name: foo }, { name: bar }, { name: test }]
---

