---
layout: staff-profile
first_name: Hope
last_name: Hall
display_name: 'Hope Hall'
email: HopeH@faithpromise.org
phone: null
title: 'fpKids Elementary Director'
teams: blount
sort: '40'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Hope Hall joined the Blount County fpKids Team as the Elementary Director. She was a Middle School Teacher for five years before joining the staff of TrueNorth church where she served as the Children's Director for two years. She moved to Tennessee from North Augusta, SC, in 2009.

Hope has been married to Dustin for thirteen years, and they have five children: Carson - 10, Abby - 9, Tristan - 7, Bryson - 6, and Maddy - 4. She loves children and is very excited about building a biblical foundation for families at the Faith Promise Blount Campus.