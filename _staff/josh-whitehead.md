---
layout: staff-profile
first_name: Josh
last_name: Whitehead
display_name: 'Dr. Josh Whitehead'
email: JoshW@faithpromise.org
phone: '1701'
title: 'Executive Pastor'
teams: 'executive,administration'
sort: '2'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Josh became Executive Pastor in 2005, during a time when the church had plateaued in attendance. Since that time, Faith Promise has almost doubled in weekly attendance.

Josh's goal is to lead by implementing the vision and values of the church through the staff and ministries. He doubled the staff in 3 years and developed comprehensive hiring, coaching, development, and evaluation plans.

Josh has been married to Kim for ten years and they have 2 children - Hayden Thomas (7) and Madison Kaye (5).

You can keep up with Josh on his blog at <a href="http://joshuawhitehead.net">JoshuaWhitehead.net</a>.