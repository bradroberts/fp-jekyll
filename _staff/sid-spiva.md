---
layout: staff-profile
first_name: Sid
last_name: Spiva
display_name: 'Sid Spiva'
email: SidS@faithpromise.org
phone: null
title: 'Anderson Campus Pastor'
teams: anderson
sort: '1'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Sid is a lifelong resident of Clinton and Anderson County. After working in Anderson County Schools for 32 years as a High School Teacher and Principal, Sid entered full time ministry in 2008. He joined the Faith Promise staff when the Anderson County Campus opened in 2013.

Sid and his wife Judy of 40 years have two outstanding sons, two wonderful daughter-in-laws, and two awesome grandsons.

He enjoys traveling, riding motorcycles, music, and spending time with family.