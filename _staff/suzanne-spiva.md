---
layout: staff-profile
first_name: Suzanne
last_name: Spiva
display_name: 'Suzanne Spiva'
email: SuzanneS@faithpromise.org
phone: null
title: 'Campbell Campus fpKids Director'
teams: campbell
sort: '5'
test: [{ name: foo }, { name: bar }, { name: test }]
---

