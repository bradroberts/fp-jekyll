---
layout: staff-profile
first_name: Brenda
last_name: Moore
display_name: 'Brenda Moore'
email: BrendaM@faithpromise.org
phone: '1000'
title: Receptionist
teams: administration
sort: '37'
test: [{ name: foo }, { name: bar }, { name: test }]
---

I was married to my high school sweetheart for 37 1/2 years. My late husband, Terry, and I served in the ministry together for over 30 years. We have four grown sons and five grandchildren (3 boys and 2 girls).

Terry and Pastor Chris were friends and frequently met together. Terry often said, "If we didn't have a church of our own, we would be at Faith Promise." When he was killed in an accident on July 4th of 2009, I realized that I was too distracted to worship at the church my husband and I had started in Clinton, TN. So in the fall of 2009, I began to attend Faith Promise on my own.

I felt God calling me to be a part of Faith Promise's staff when I began to feel a restlessness in my spirit at my former job. As I prayed about that restlessness, it became very clear to me that I was to be on staff, but I didn't know where God was going to put me. One morning during my quiet time, I felt the restlessness in an even greater way. I asked God, "What are you trying to tell me? Why am I suddenly so discontent where I am?"

That very afternoon I got a call from my youngest son who told me he had been on FP's website and had found the perfect job for me. I went home, got online, and applied for the Receptionist's position. Several interviews and a couple of months later, I received a call that I was being offered the job. I accepted on the spot. I began working at Faith Promise in the fall of 2011. What a blessing it has been to be a part of this team!

I am involved in fpWomen's Groups as part of the Leadership Team. I also lead women's Bible studies and serve as a baptism assistant.

In my spare time, I love to be with my family-especially with my grandchildren. I also love to read and travel and have a heart for missions.

I am deathly afraid of snakes! I have been known to do a wicked snake dance upon spotting one.