---
layout: staff-profile
first_name: Mike
last_name: Baker
display_name: 'Mike Baker'
email: MikeB@faithpromise.org
phone: null
title: 'North Knoxville Campus Pastor'
teams: north
sort: '3'
test: [{ name: foo }, { name: bar }, { name: test }]
---

