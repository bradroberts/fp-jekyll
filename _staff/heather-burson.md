---
layout: staff-profile
first_name: Heather
last_name: Burson
display_name: 'Heather Burson'
email: HeatherB@faithpromise.org
phone: '1014'
title: 'Graphic Designer'
teams: administration
sort: '33'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Hi, I'm Heather. I'm a native East Tennessean, graduate of the University of Tennessee, and a die-hard Vols fan. My husband, Mike, and I enjoy travel, college football, and a little friendly competition now and again.

On staff since 2007, I love being a member of the Communications Team (part of the Admin staff) and a keeper of the Faith Promise brand. One of my favorite things about designing at Faith Promise is the variety of projects that come my way, but by far the best thing is that is I get to be part of an awesome team that is impacting the world for Christ.