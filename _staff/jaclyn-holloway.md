---
layout: staff-profile
first_name: Jaclyn
last_name: Holloway
display_name: 'Jaclyn Holloway'
email: JaclynH@faithpromise.org
phone: '1503'
title: 'Assistant to the Worship and Creative Arts Leader'
teams: worship
sort: '6'
test: [{ name: foo }, { name: bar }, { name: test }]
---

A beach girl at heart that fell in love with the mountains, Jaclyn moved to Knoxville from Virginia Beach in 2005. Jaclyn has a B.S. in Communications from the University of Tennessee with a focus on video production. She has been a part of the worship ministry staff since 2011 and feels extremely blessed to be part of such a talented and creative team. She enjoys scriptwriting and acting as part of her ministry, especially when humor is involved.

When she goes home, she gets to play her favorite role: a wife and mother. Her husband, Phil Holloway is also involved in the worship ministry as the Anderson Campus Worship Pastor. They are blessed with two boys, Jameson and Jude. Favorite family hobbies are having dance parties with Yo Gabba Gabba, drag racing hot wheels, and synchronized swimming in the kiddie pool. Follow their adventures on twitter with @HollowayJaclyn.