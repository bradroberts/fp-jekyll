---
layout: staff-profile
first_name: Jeff
last_name: Rose
display_name: 'Jeff Rose'
email: JeffR@faithpromise.org
phone: null
title: 'Building Services Director'
teams: administration
sort: '99'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Jeff has been a member of FPC since the very beginning of the church, and has served in various areas before joining the staff in September 2012. Jeff's background includes Maintenance Supervisor for the Boys & Girls Club of TN Valley. He loves sports, traveling, and action movies.

He and his wife Robyn live in the Hardin Valley area with their two dogs Sophie and Charlie. They have one daughter, Jennifer, who is also on FPC staff as Assistant to the Pastor of Family Ministries.