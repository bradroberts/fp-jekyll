---
layout: staff-profile
first_name: Kandice
last_name: Baker
display_name: 'Kandice Baker'
email: KandiceB@faithpromise.org
phone: '1103'
title: 'North Knoxville Campus fpKids Director'
teams: 'family,north'
sort: '8'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Kandice grew up in Johnson City, Tennessee, and graduated at the University of Tennessee. While in college, she met and married the love of her life, Mike, who led her to the Lord.

They were soon after blessed with a baby girl, and their family grew to five kids. Kandice was a stay-at-home mom for many years.

Her experience as a mother of five children allows her to connect with families on a personal level as she relates to everyday life and the challenges of raising kids. Her passion is to connect people to God, to each other, and to service. She especially loves hearing the stories of young people accepting Jesus.

Kandice joined the fpKids staff in 2003 and became the North Knoxville Children's Ministry Director in January 2012. Her husband, Mike, serves as the North Knoxville Campus Pastor of Faith Promise.