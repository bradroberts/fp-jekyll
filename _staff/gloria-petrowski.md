---
layout: staff-profile
first_name: Gloria
last_name: Petrowski
display_name: 'Gloria Petrowski'
email: GloriaP@faithpromise.org
phone: '2000'
title: 'Assistant to the Senior Pastor'
teams: administration
sort: '6'
test: [{ name: foo }, { name: bar }, { name: test }]
---

I was born in Oak Ridge and have never lived anywhere else. I was raised in a loving Christian home and was saved and baptized at the age of nine. I have one younger sister, Lisa, who lives in Snellville, GA. My parents are both deceased.

I have been married to my awesome husband, Larry, for 36 years. God blessed us with two wonderful children - Justin, 27 and Jenna, 24 - and their equally wonderful spouses - Kara, 26 and Ben, 25. We all serve the Lord here together in various ways in the Worship Ministry of Faith Promise. I began singing in the Cherub Choir at the age of three and have been singing since that time!

I was the first employee hired here when Faith Promise began in 1995. I worked first in finance and membership and then became Pastor Chris' personal assistant when he joined us in 1996. What a joy it is to serve alongside him and to see all the many miracles God continues to perform here!

I enjoy walking, boating, reading, and especially traveling. If someone says "go", I am always ready! I love hot chocolate-chip cookies and don't like greens! I don't like heights, but through the years I have learned to conquer that fear somewhat. My nickname growing up was Glory or Gloworm and I can speak pig-Latin really fast!