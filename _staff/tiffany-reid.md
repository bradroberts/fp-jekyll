---
layout: staff-profile
first_name: Tiffany
last_name: Reid
display_name: 'Tiffany Reid'
email: TiffanyR@faithpromise.org
phone: null
title: 'fpKids Team Administrator'
teams: family
sort: '11'
test: [{ name: foo }, { name: bar }, { name: test }]
---

