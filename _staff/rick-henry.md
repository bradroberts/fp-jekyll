---
layout: staff-profile
first_name: Rick
last_name: Henry
display_name: 'Rick Henry'
email: RickH@faithpromise.org
phone: null
title: 'Campbell Campus Groups Director'
teams: campbell
sort: '6'
test: [{ name: foo }, { name: bar }, { name: test }]
---

