---
layout: staff-profile
first_name: Dustin
last_name: Scott
display_name: 'Dustin Scott'
email: DustinS@faithpromise.org
phone: null
title: 'Pastor of Group Leader Discipleship'
teams: groups
sort: '80'
test: [{ name: foo }, { name: bar }, { name: test }]
---

