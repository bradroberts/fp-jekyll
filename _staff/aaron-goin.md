---
layout: staff-profile
first_name: Aaron
last_name: Goin
display_name: 'Aaron Goin'
email: AaronG@faithpromise.org
phone: '1602'
title: 'Chief Financial Officer'
teams: administration
sort: '10'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Aaron Goin joined the staff of Faith Promise as Finance Director in August 2007, and in January 2011, Aaron took on the role of Chief Financial Officer. In this role he gets to work alongside great teams in Finance, Human Resources, Information Technology, and Facilities.

Aaron and his wife Kerri were married in 2000. Their daughter Grace was born in April 2006 and their son Mason was born in August of 2009. They love all things Disney, Dollywood, the beach, and any other kind of family adventure.

In 2002, Aaron graduated from the University of Tennessee with a bachelor's degree in Finance. He received his Master of Business Administration degree in December 2010. Before joining the staff of Faith Promise, he worked for five years at Oak Ridge National Laboratory as a Subcontract Administrator.

"It has been an amazing journey to watch Faith Promise grow and to play a small part in that growth. Can't wait to see what God does as we continue to reach the world for Him."