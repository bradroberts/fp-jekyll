---
layout: staff-profile
first_name: Chuck
last_name: Carringer
display_name: 'Dr. Chuck Carringer'
email: ChuckC@faithpromise.org
phone: '1101'
title: 'Pastor of Leadership Development & Stewardship'
teams: 'executive,leadership'
sort: '5'
test: [{ name: foo }, { name: bar }, { name: test }]
---

In my role as Pastor of Leadership Development and Stewardship I partner with staff and volunteers to increase leadership capacity and stewardship across Faith Promise. From 2009-2014 I served as Pastor of Family Ministry. Prior to joining the staff I served for 24 years in public schools. From 1990-2009 I was part of the faculty at Oak Ridge High School serving in a variety of roles including: teacher, coach, vice principal, athletic director, and principal.

My wife Emily, of 25 years, and I have been members of Faith Promise since the church began. I volunteered in a number of ministry areas including serving as an elder, student ministry, school of leaders, and membership classes.

In addition to my role at Faith Promise I provide executive coaching to business leaders. I have a doctorate in executive leadership as well as being a John Maxwell Team Certified Coach, Speaker and Trainer. In addition to executive coaching, I speak frequently to business teams and organizations.

I enjoy family time, and Em, Zach (20 years old), and Maggie (17 years old). We love traveling, especially to the beach! In addition to family fun I enjoy sports, reading, exercising, and time with friends.

Follow Chuck on <a href="http://twitter.com/chuckcarringer" target="blank">Twitter</a>, <a href="http://www.facebook.com/chuckcarringer" target="_blank">Facebook</a>, or <a href="http://www.chuckcarringer.com" target="_blank">his blog</a>.