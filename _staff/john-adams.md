---
layout: staff-profile
first_name: John
last_name: Adams
display_name: 'John Adams'
email: JohnA@faithpromise.org
phone: null
title: 'Blount Campus Worship Pastor'
teams: blount
sort: '7'
test: [{ name: foo }, { name: bar }, { name: test }]
---

