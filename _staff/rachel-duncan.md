---
layout: staff-profile
first_name: Rachel
last_name: Duncan
display_name: 'Rachel Duncan'
email: RachelD@faithpromise.org
phone: null
title: 'Assistant to Worship and Creative Arts Leader'
teams: worship
sort: '5'
test: [{ name: foo }, { name: bar }, { name: test }]
---

