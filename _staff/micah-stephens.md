---
layout: staff-profile
first_name: Micah
last_name: Stephens
display_name: 'Micah Stephens'
email: MicahS@faithpromise.org
phone: null
title: 'Creative Director'
teams: worship
sort: '10'
test: [{ name: foo }, { name: bar }, { name: test }]
---

