---
layout: staff-profile
first_name: Kyle
last_name: Gilbert
display_name: 'Kyle Gilbert'
email: KyleG@faithpromise.org
phone: '1704'
title: 'Pastor of Communications & Internet Campus Pastor'
teams: administration
sort: '32'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Kyle came on staff at Faith Promise Church in October 2008 as the Pastor of Communications. He oversees web technology, graphic design, the Internet Campus, and helping get the word out about our church.

He and his wife, Keri, were married in 1999 and haven't owned a TV since then. They have four crazy-awesome kids, including a set of twins.

Kyle has gone skydiving, and he once went scuba diving in the Black Sea after five minutes of instruction (in Russian).

For fun, Kyle loves to play disc golf and racquetball, and he plays both poorly but with enthusiasm.

You can keep up with Kyle on his blog at <a href="http://kylesrandom.com">KylesRandom.com</a> or <a href="http://twitter.com/kylegilbert">follow him on Twitter</a>.