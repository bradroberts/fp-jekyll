---
layout: staff-profile
first_name: Mallory
last_name: Ellis
display_name: 'Mallory Ellis'
email: MalloryE@faithpromise.org
phone: null
title: 'Accounting Director & Human Resources Director'
teams: administration
sort: '11'
test: [{ name: foo }, { name: bar }, { name: test }]
---

I'm from Jackson, TN. I grew up with an amazing godly family who took us to church every time the doors were open and taught me the importance of serving. I moved to Knoxville in 2004 to attend UT, where I majored in pretty much everything but finally settled on Accounting. :)
 
I met Mike my first semester here, and we quickly became friends. We didn't start dating until we worked together as missionaries the summer of 2007 at Montgomery Village Baptist Center. We were both interns at our local church, and after we got married in May 2009, a couple (the Beukemas) invited us to FPC, and it has been AWESOME to be a part of this church!