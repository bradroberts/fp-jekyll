---
layout: staff-profile
first_name: Michelle
last_name: Hearon
display_name: 'Michelle Hearon'
email: MichelleH@faithpromise.org
phone: null
title: 'Blount Campus Administrative Assistant'
teams: blount
sort: '50'
test: [{ name: foo }, { name: bar }, { name: test }]
---

