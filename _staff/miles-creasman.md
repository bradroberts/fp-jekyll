---
layout: staff-profile
first_name: Miles
last_name: Creasman
display_name: 'Miles Creasman'
email: MilesC@faithpromise.org
phone: '1005'
title: 'Next Steps Pastor/Blount Groups Pastor'
teams: 'administration,blount'
sort: '34'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Miles Creasman has been on staff as the Blount Campus Groups Director since June 2010 and also took on the role of Pastor of Next Steps in the middle of 2011. Originally from Riceville, TN, Miles is a graduate of Carson-Newman College and Southern Baptist Theological Seminary in Louisville, KY. He has been in ministry for 15 years and has served on church staffs as Recreation Minister, Middle & High School Pastor, and Singles Pastor.

Miles has a passion to help people grow in their relationship with the Lord, and he believes that the context of community found in groups is the best place for that to happen.

Miles has been married to Katy for 13 years, and they have 3 children - Jess (9), Will (7), and Leah (3).

Miles' favorite hobby is playing with his kids. He loves playing Frisbee golf & fantasy sports. Miles and Katy are both very big UT Vols fans and love to go to games whenever they can.