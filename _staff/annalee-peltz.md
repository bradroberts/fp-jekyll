---
layout: staff-profile
first_name: AnnaLee
last_name: Peltz
display_name: 'AnnaLee Peltz'
email: AnnaLeeP@faithpromise.org
phone: null
title: 'Volunteer Coordinator-Elementary Small Groups'
teams: family
sort: '13'
test: [{ name: foo }, { name: bar }, { name: test }]
---

