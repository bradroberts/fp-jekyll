---
layout: staff-profile
first_name: Ann
last_name: Slimp
display_name: 'Ann Slimp'
email: AnnS@faithpromise.org
phone: null
title: 'Staff Counselor'
teams: administration
sort: '200'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Ann is a licensed psychologist who integrates her faith in God with her professional skills to minister to people through her role as a counselor.

She received her Ph.D. in psychology, at Auburn University in 1991 and has been licensed in TN since 1992.

She has two awesome children - a daughter who is 18 and a son who is 15. They continue to be her greatest teachers.

She has been a Christian since her childhood and continues to be astounded at God's abundant grace in her life. 