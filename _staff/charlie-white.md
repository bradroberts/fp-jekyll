---
layout: staff-profile
first_name: Charlie
last_name: White
display_name: 'Charlie White'
email: CharlieW@faithpromise.org
phone: null
title: 'Anderson Campus fpKids Director'
teams: anderson
sort: '5'
test: [{ name: foo }, { name: bar }, { name: test }]
---

