---
layout: staff-profile
first_name: Noah
last_name: Case
display_name: 'Noah Case'
email: NoahC@faitpromise.org
phone: '1306'
title: 'North Knox Student Pastor'
teams: north
sort: '3'
test: [{ name: foo }, { name: bar }, { name: test }]
---

