---
layout: staff-profile
first_name: Aimee
last_name: Fair
display_name: 'Aimee Fair'
email: AimeeF@faithpromise.org
phone: null
title: 'fpKids Preschool Director'
teams: blount
sort: '41'
test: [{ name: foo }, { name: bar }, { name: test }]
---

