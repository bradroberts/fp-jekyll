---
layout: staff-profile
first_name: Zach
last_name: Carringer
display_name: 'Zach Carringer'
email: ZachC@faithpromise.org
phone: null
title: 'Worship Intern'
teams: worship
sort: '900'
test: [{ name: foo }, { name: bar }, { name: test }]
---

