---
layout: staff-profile
first_name: Emily
last_name: Carringer
display_name: 'Emily Carringer'
email: EmilyC@faithpromise.org
phone: '1500'
title: 'Worship Programming Director'
teams: worship
sort: '28'
test: [{ name: foo }, { name: bar }, { name: test }]
---

My husband, Chuck, and I have been at Faith Promise Church since the very first service at the Garden Plaza Hotel in Oak Ridge. However, I didn't join the FPC staff until 2003.

It has been an adventure ever since. You really never know what each day will hold. We have two great kids that have grown up at FPC. Zach is 14 and Maggie is 11. When we're not at church, we love eating out and hanging out together as a family.

I love to read, have fun, and laugh a lot, usually at myself. One of the coolest parts about what I get to do is to help others be a part of the Worship Ministry and serve where they are passionate.