---
layout: staff-profile
first_name: Ryan
last_name: Silowski
display_name: 'Ryan Silowski'
email: RyanS@faithpromise.org
phone: null
title: 'Anderson Campus Student Pastor'
teams: anderson
sort: '10'
test: [{ name: foo }, { name: bar }, { name: test }]
---

I was born in New Orleans, LA, and moved to east Tennessee in the early 2000's. I was home schooled for all twelve years of my primary education, and after high school I went to Johnson Bible College (now Johnson University).

I graduated in 2013 with a Bachelors of Science in Biblical Studies and Digital Video Production. I have always had a love for storytelling, and video is the primary way we communicate stories in this day in age. I have loved films and filmmaking for some time.

In college I spent two summers in Seattle, WA, and it quickly became a second home for me. In my senior year of college, I had the privilege to be a Director of Photography for a small Christian feature film made in the Knoxville area. I have also participated in several competitions for the Knoxville film festival.

Through some of my internships in college, I became the Student Pastor at Life Point Church. In September of 2013, Life Point became a Faith Promise Campus, and I stayed on in my role of Student Pastor.