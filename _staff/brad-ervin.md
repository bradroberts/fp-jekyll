---
layout: staff-profile
first_name: Brad
last_name: Ervin
display_name: 'Brad Ervin'
email: BradE@faithpromise.org
phone: '1405'
title: 'Pastor of Outreach and Missions'
teams: groups
sort: '49'
test: [{ name: foo }, { name: bar }, { name: test }]
---

Brad Ervin is the Pastor of Outreach and Missions at Faith Promise. He joined the staff in Nov 2007, and he and his wife Caroline have been attending faith promise since February of 2006. Brad is a huge Vol fan and loves sports in general. He graduated with a BS in Business Marketing from Samford University in 2001.

Hobbies include: reading (Tolkien/Ayn Rand/Robert Jordan some favs), travel, music (especially British Rock), movies, broadway and west end musicals, and sucking the marrow out of life. He is also an accomplished break dancer and pole vaulter.

Facebook him Brad Ervin, twitter: BradErvin and myspace: Volcrazy.